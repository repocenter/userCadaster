package main

import(
	"io"
	"encoding/json"
)

type User struct{
	Nome string	`json:"name"`
	Idade int 	`json:"age"`
	Email string 	`json:"email"`
}

var users []User


//////////////////////////////////////////
//func that insert new users on database//
//////////////////////////////////////////

func Insert(body io.ReadCloser)[]User{
	var tempUser User
	json.NewDecoder(body).Decode(&tempUser)
	
	users = append(users, tempUser)
	
	return users
	
}

////////////////////////////////////////////////////////////
//func that return a boolean for an user existence request//
////////////////////////////////////////////////////////////

func SearchByName(name string)bool{
	for i:= 0; i < len(users);i++{
		if users[i].Nome == name{
			return true
		}
	}
	return false
}
