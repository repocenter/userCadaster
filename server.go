package main

import(
	"net/http"
	"time"
	"log"
	"encoding/json"
)

type Router struct{}

func (Router) ServeHTTP(res http.ResponseWriter, req *http.Request){
	path := req.URL.Path
	
	if path == "/user" && req.Method == "POST"{
		resp := Insert(req.Body)
		ship, err := json.Marshal(resp)
		if err != nil{
			res.Write([]byte("Erro no jason"))
		}
		res.Write(ship)
		return 
	}
	
	if path == "/user" && req.Method == "GET"{
		name := req.URL.Query().Get("name")
		resp := SearchByName(name)
		ship, err := json.Marshal(resp)
		if err != nil{
			res.Write([]byte("Erro no jason"))
		}
		res.Write(ship)
	}	
}

func RunService(){
	s := &http.Server{
		Addr:	"127.0.0.1:8080",
		Handler: 	Router{},
		ReadTimeout:	10*time.Second,
		WriteTimeout:   10*time.Second,
	}
	
	log.Fatal(s.ListenAndServe())
}
